﻿
using Kongt.IServices.QueryModel;
using Kongt.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kongt.IServices
{
    public interface ICustomService : IDisposable, IService
    {
        void Create(Customer custom);
        Customer Find(long id);
        void Update(Customer custom);
        void Delete(long id);
        PageModel<Customer> QueryCustomers(CustomQuery Query);

        void CreateAccount(OpenAccountInfo account);
        OpenAccountInfo FindAccount(long id);
        void UpdateAccount(OpenAccountInfo oai);
        void DeleteAccount(long id);
        PageModel<OpenAccountInfo> QueryAccounts(OpenAccountQuery Query);














    }
}