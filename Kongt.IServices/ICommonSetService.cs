﻿
using Kongt.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kongt.IServices
{
    public interface ICommonSetService : IDisposable, IService
    {
        #region 居住区域
        void CreateArea(Area area);
        void UpdateArea(Area area);
        void DeleteArea(long areaId);
        Area FindArea(long id);
        PageModel<Area> QueryArea(QueryBase Query);
        List<String> GetSecondAreaByFirstArea(String firstArea);
        List<String> GetFirstArea();
        #endregion

        #region 用水类别
        void CreateWaterType(WaterType type);
        void UpdateWaterType(WaterType type);
        void DeleteWaterType(long WaterTypeId);
        WaterType FindWaterType(long WaterTypeId);
        PageModel<WaterType> QueryWaterType(QueryBase Query);

        #endregion

        #region 抄表人员

        void CreateEmployee(EmployeeInfo employee);
        void UpdateEmployee(EmployeeInfo employee);
        void DeleteEmployee(long employeeId);
        EmployeeInfo FindEmployee(long id);
        PageModel<EmployeeInfo> QueryEmployee(QueryBase Query);
        #endregion

        #region 附加费
        void CreateExtendCharge(ExtendCharge extendCharge);
        void UpdateExtendCharge(ExtendCharge extendCharge);
        void DeleteExtendCharge(long id);
        ExtendCharge FindExtendCharge(long id);
        PageModel<ExtendCharge> QueryExtendCharge(QueryBase Query);
        #endregion

        #region 材料名称
        void CreateDocument(Document doc);
        void UpdateDocument(Document doc);
        void DeleteDocument(long id);
        Document FindDocument(long id);
        PageModel<Document> QueryDocument(QueryBase Query);
        #endregion


        #region 滞纳金设置
        void CreateLateFee(LateFee fee);
        void UpdateLateFee(LateFee doc);
        void DeleteLateFee(long id);
        LateFee FindLateFee(long id);
        PageModel<LateFee> QueryLateFee(QueryBase Query);
        #endregion


        #region 阶梯水价
        void CreateLadderFee(LadderFee fee);
        void UpdateLadderFee(LadderFee doc);
        void DeleteLadderFee(long id);
        LadderFee FindLadderFee(long id);
        PageModel<LadderFee> QueryLadderFee(QueryBase Query);
        #endregion




        /// <summary>
        /// 创建减免
        /// </summary>
        /// <param name="fee"></param>
        void CreateFreeSet(Freeset fee);

        /// <summary>
        /// 更新减免
        /// </summary>
        /// <param name="fee"></param>
        void UpdateFreeSet(Freeset fee);
        /// <summary>
        /// 删除减免
        /// </summary>
        /// <param name="id"></param>
        void DeleteFreeSet(long id);

        /// <summary>
        /// 逐渐查找减免
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Freeset FindFreeSet(long id);

        /// <summary>
        /// 查询减免
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        PageModel<Freeset> QueryFreeSet(QueryBase Query);

    }
}