﻿
using Kongt.IServices.QueryModel;
using Kongt.Model;
using Kongt.Model.Custom;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kongt.IServices
{
    public interface IBuyWaterService : IDisposable, IService
    { 
        void Create(BuyWaterInfo ci);
        void Update(BuyWaterInfo ci);
        void Delete(long id);
        BuyWaterInfo Find(long id);
        PageModel<BuyWaterInfo> QueryBuyWaterInfoes(BuyWaterQuery Query);

    }
}