﻿using Kongt.IServices.QueryModel;
using Kongt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kongt.IServices
{
    public interface IMeterService : IDisposable, IService
    {
        void Create(MeterInfo custom);
        MeterInfo Find(long id);
        void Update(MeterInfo custom);
        void Delete(long id);
        PageModel<MeterInfo> QueryMeters(MeterInfoQuery Query);



        void CreateChangeMeter(ChangeMeterInfo cm);
        ChangeMeterInfo FindChangeMeter(long id);
        void UpdateChangeMeter(ChangeMeterInfo custom);
        void DeleteChangeMeter(long id);
        PageModel<ChangeMeterInfo> QueryChangeMeters(ChangeMeterQuery Query);
        PageModel<Customer> QueryNoReadMeterCustomers(NoReadMeterQuery Query);



    }
}
