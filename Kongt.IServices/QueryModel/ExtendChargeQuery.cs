﻿using Kongt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Kongt.IServices.QueryModel
{
    public class ExtendChargeQuery
    {
        public Nullable<long> WaterTypeId { get; set; }
        public string HasMeter { get; set; }
        public int? ChargeType { get; set; }
        public string ExtendName { get; set; }
        public string FirstArea { get; set; }
        public string SecondArea { get; set; }
        public string DetailAddress { get; set; } 
    }
}
