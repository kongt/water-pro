﻿using Kongt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Kongt.IServices.QueryModel
{
    public class CustomQuery : QueryBase
    {
        public string CustomerId { get; set; }
        public string EndCustomerId { get; set; }
        public string CustomerName { get; set; }
        public string PersonId { get; set; }
        public string FirstArea { get; set; }
        public string SecondArea { get; set; }
        public string LinkPhone { get; set; }
        public string MeterNo { get; set; }

        public Customer.CustomStatEnum? CustomStat { get; set; }

        public Customer.HouseTypeEnum? HouseType { get; set; }

        public Customer.MeterNatureEnum? MeterNature { get; set; }

        public long? EmployeeId { get; set; }

        public DateTime? OpenAccountDate { get; set; }

        public long? WaterTypeId { get; set; }
        public float? MeterStart { get; set; }
    }
}
