﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kongt.IServices.QueryModel
{

    public class MeterInfoQuery : QueryBase
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string MeterNo { get; set; }
        public string FirstArea { get; set; }
        public string SecondArea { get; set; }
        public long? WaterTypeId { get; set; }

        public long? ReadPersonId { get; set; }
        public DateTime? ReadMatchDateStart { get; set; }
        public DateTime? ReadMatchDateEnd { get; set; }
    }
}
