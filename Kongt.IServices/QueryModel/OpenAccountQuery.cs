﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kongt.IServices.QueryModel
{
    public class OpenAccountQuery : QueryBase
    {

        /// <summary>
        /// 这里指用户的CustomerId str
        /// </summary>
        public string customerId { get; set; }

        /// <summary>
        /// 用户主键ID  customer的主键ID
        /// </summary>
        public long? UserId { get; set; }
    }
}
