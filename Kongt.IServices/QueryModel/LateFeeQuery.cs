﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Kongt.IServices.QueryModel
{
    /// <summary>
    /// 滞纳金
    /// </summary>
    public class LateFeeQuery
    {
        /// <summary>
        /// 用水类型
        /// </summary>
        public long CustomerId { get; set; }

        /// <summary>
        /// 水费总额 （用于判断是否达到计算滞纳金最低标准）
        /// </summary>
        public decimal TotalWaterPrice { get; set; }

        /// <summary>
        ///哪个月的水费
        /// </summary>
        public DateTime ChargeWhichDate { get; set; }

        /// <summary>
        /// 缴费日期：用于计算滞纳天数：计算方式：（应该缴费的月份不计算滞纳金，由应该缴费的次月第一日开始计算滞纳天数）
        /// </summary>
        public DateTime ColetMoneyDate { get; set; }





    }
}
