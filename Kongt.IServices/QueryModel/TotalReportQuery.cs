﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Kongt.IServices.QueryModel
{

    /// <summary>
    /// 汇总脱机参数模型
    /// </summary>
    public class TotalReportQuery : QueryBase
    {
        public string FirstArea { get; set; }
        public string SecondArea { get; set; }

        public DateTime? dateStart { get; set; }

        public DateTime? dateEnd { get; set; }


    }
}
