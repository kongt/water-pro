﻿namespace Kongt.IServices
{
    using System;
    using System.Runtime.CompilerServices;

    public class QueryBase
    {

        public bool IsAsc { get; set; }

        public int PageIndex { get; set; } = 1;

        public int PageSize { get; set; } = 15;

        public string Sort { get; set; }



        public int Skip
        {
            get
            {
                return this.PageSize * (this.PageIndex - 1);
            }
        }
    }
}

