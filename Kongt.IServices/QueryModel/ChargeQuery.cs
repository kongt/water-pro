﻿using Kongt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Kongt.IServices.QueryModel
{
    public class ChargeQuery : QueryBase
    {

        /// <summary>
        /// 用户ID
        /// </summary>
        public long? CustomId { get; set; }

        /// <summary>
        /// 按照缴费状态
        /// </summary>
        public ChargeInfo.StateEnum? Stat { get; set; }

        /// <summary>
        /// 收费类型
        /// </summary>
        public ChargeInfo.CollectMoneyTypeEnum? colectType { get; set; }


        /// <summary>
        /// 收费时间段开始日期
        /// </summary>
        public DateTime? CollectDateStart { get; set; }


        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime? CollectDateEnd { get; set; }


        /// <summary>
        /// 收费员
        /// </summary>
        public long? Collector { get; set; }


        public string UserCustomerId { get; set; }


    }
}
