﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Kongt.IServices.QueryModel
{
    public class LadderFeeQuery
    {
        /// <summary>
        /// 用水量
        /// </summary>
        public double WaterMuch { get; set; }//用水量

        /// <summary>
        /// 用户ID
        /// </summary>
        public long CustomerId { get; set; }
    }
}
