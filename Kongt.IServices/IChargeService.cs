﻿
using Kongt.IServices.QueryModel;
using Kongt.Model;
using Kongt.Model.Custom;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kongt.IServices
{
    public interface IChargeService : IDisposable, IService
    {

        /// <summary>
        /// 更新账单状态【付款/未付款】
        /// </summary>
        /// <param name="Stat"></param>
        /// <param name="Ids"></param>
        void UpdateChargeInfos(ChargeInfo.StateEnum Stat, ChargeInfo.CollectMoneyTypeEnum? colectType, long? colector, params long[] Ids);

        /// <summary>
        /// 获得附加费
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        decimal GetExtendCharge(ExtendChargeQuery Query);

        /// <summary>
        /// 获得水价
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        decimal GetLadderFeeCharge(LadderFeeQuery Query);


        /// <summary>
        /// 计算滞纳金
        ///  先按照滞纳天数 与利率计算滞纳金 如果没有达到最低标准 按照最低标准交滞纳金
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        decimal GetLateFeeCharge(LateFeeQuery Query);


        /// <summary>
        /// 获得当前用户指定时间的前一次缴费账单信息
        /// </summary>
        /// <param name="thisReadMatchTime"></param>
        /// <returns></returns>
        ChargeInfo GetPrevChargeInfo(long CustomerId,DateTime dtEnd);

        /// <summary>
        /// 根据消费账单名称 删除消费信息
        /// </summary>
        /// <param name="chargeName"></param>
        void DeleteChargeByChargeName(string chargeName);


        void Create(ChargeInfo ci);
        void Update(ChargeInfo ci);
        void Delete(long id);
        ChargeInfo Find(long id);
        PageModel<ChargeInfo> QueryChargeInfoes(ChargeQuery Query);
         
        /// <summary>
        /// 获得汇总统计
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        TotalReportModel GetTotalReportModel(TotalReportQuery Query);

    }
}