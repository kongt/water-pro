﻿
using Kongt.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kongt.IServices
{
    public interface IManagerService : IDisposable, IService
    {
        ManagerInfo GetManagerByUserName(String userName);

        void Create(ManagerInfo manager);
         
        PageModel<ManagerInfo> Query(QueryBase q);

    }
}