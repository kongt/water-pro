--管理员表
CREATE TABLE ManagerInfo(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
UserName VARCHAR(50) NOT NULL,
PASSWORD VARCHAR(50) NOT NULL,
Roles	VARCHAR(500) NULL,
CreateDate	DATETIME NOT NULL
)
--基础设置--

--居住区域
CREATE TABLE Area(
	Id bigint IDENTITY(1,1) NOT NULL PRIMARY KEY,
	FirstArea nvarchar(500) NOT NULL,
	SecondArea nvarchar(500) NOT NULL,
	DetailAddress nvarchar(500) NULL
 )

--用水类别
CREATE TABLE WaterType
(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
TypeName NVARCHAR(50) NOT NULL,
MeterType INT NOT NULL,
NoMeter INT NOT NULL,
MeterUse NVARCHAR(50) NULL,
Waterprice MONEY NOT NULL,
Ladder BIT NOT NULL,
Description nvarchar(500) NULL 
)
--抄表人员
CREATE TABLE EmployeeInfo(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
EmployeeNo VARCHAR(50) NOT NULL,
NAME NVARCHAR(50) NOT NULL,
Sex  bit NOT NULL,
PersonID VARCHAR(50) NULL,
FirstArea NVARCHAR(500) NULL,
SecondArea NVARCHAR(500) NULL,
DetailAddress NVARCHAR(500) NULL,
Phone VARCHAR(50) NULL,
Description nvarchar(500) null 
)
--附加费
CREATE TABLE ExtendCharge
(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
WaterTypeId bigint NULL REFERENCES WaterType(Id),
HasMeter	VARCHAR(50) NULL,
ChargeType INT NOT NULL,
ExtendName VARCHAR(500),
FirstArea NVARCHAR(500) NULL,
SecondArea NVARCHAR(500) NULL,
DetailAddress NVARCHAR(500) NULL,
ExtendPrice MONEY NOT NULL,
DESCRIPTION NVARCHAR(500) 
)
--材料设置
CREATE TABLE Document
(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
DocumentNo VARCHAR(500) NOT NULL,
DocumentName VARCHAR(500) NOT NULL,
DocumentUnit VARCHAR(500) NOT NULL,
DocumentPrice MONEY NOT NULL
)
--滞纳金设置
CREATE TABLE LateFee(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
WaterTypeId BIGINT NULL REFERENCES WaterType(Id),
GetState BIT NOT NULL,
ChargeType INT NOT NULL,
LevelStand VARCHAR(500) NULL,
TaxRate FLOAT NOT NULL,
StartDayOnMonth INT NOT NULL,
EffectiveDate datetime NOT NULL,
StartDay VARCHAR(500) NULL,
EndDay VARCHAR(500) NULL
)
--阶梯水价设置
CREATE TABLE LadderFee(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
WaterTypeId BIGINT NULL REFERENCES WaterType(Id),
START FLOAT NOT NULL,
EndTo float NOT NULL,
Price	MONEY NOT NULL,
DESCRIPTION VARBINARY(500) NOT NULL
)


---用户部分-----------
CREATE TABLE Customer(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
CustomerId NVARCHAR(50) NOT NULL,
CustomerName NVARCHAR(50) NOT NULL,
PersonId VARCHAR(50) NOT NULL,
LinkPhone VARCHAR(50) NOT NULL,
FirstArea NVARCHAR(500) NULL,
SecondArea NVARCHAR(500) NULL,
DetailAddress NVARCHAR(500) NULL,
MeterNo NVARCHAR(50) NOT NULL,
MeterStart FLOAT NOT NULL,
CustomerStat INT NOT NULL,
HouseType INT NOT NULL,
WaterType NVARCHAR(50) NOT NULL,
MeterNature INT NOT NULL,
ReadPersonId BIGINT NULL REFERENCES EmployeeInfo(Id),
OpenAccountDate datetime NULL,
DESCRIPTION nvarchar(500),
ParentId bigint NULL REFERENCES Customer(Id),
CreateDate datetime NOT null
)
--开户信息
CREATE TABLE OpenAccountInfo(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
CustomerId bigint NOT NULL REFERENCES Customer(Id),
OpenAccountCharge MONEY NOT NULL,
DocumentCharge MONEY NOT NULL,
WorkTimeCharge MONEY NOT NULL,
OtherCharge MONEY NOT NULL,
ShouldGetCharge MONEY  NULL,
RealyGetCharge MONEY NULL,
RestoreCharge MONEY NULL,
OperatorId BIGINT REFERENCES ManagerInfo(Id),
CreateDate DATETIME NOT null
)
--开户材料表
CREATE TABLE OpenAccountDocument(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
OpenAccountInfoId BIGINT NOT NULL REFERENCES OpenAccountInfo(Id),
DocumentName NVARCHAR(50) NOT NULL ,
DocumentUnit NVARCHAR(50) NOT NULL,
DocumentPrice MONEY NOT NULL,
DocumentNum	INT NOT NULL,
DocumentDesc NVARCHAR(500) null
)
--电表信息
CREATE TABLE MeterInfo(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
CustomerId BIGINT NOT NULL REFERENCES Customer(Id),
LastShow FLOAT NOT NULL,
ThisShow FLOAT NOT NULL,
ThisLoseWater FLOAT NOT NULL DEFAULT(0),
ReadMeterDate DATETIME NOT NULL,
MeterStat INT NOT NULL
)

--换表信息
create table ChangeMeterInfo(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
CustomerId BIGINT NOT NULL REFERENCES Customer(Id),
NewMeterNo varchar(50) null,
Operator bigint references ManagerInfo(Id),
ChangeMeterDate datetime  not null,
MeterStart float not null,
ProcesDate datetime not null 
)

--收费管理
create table ChargeInfo(
Id BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1),
CustomerId BIGINT NOT NULL REFERENCES Customer(Id),
ChargeName varchar(500) not null,
LastTime  datetime not null,
LastMeterShow float not null,
ThisTime datetime not null,
ThisMeterShow float not null,
WatchMuch float not null,
WaterPrice money not null,
TotalWaterPrice money not null,
ExtendPrice money not null,
LateFee money not null,
TotalPricec money not null,
Stat int not null,
CollectMoneyType int not null
)