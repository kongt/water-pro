﻿namespace Kongt.Common 
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Web.Mvc;

    public static class EnumHelper
    {
        private static Hashtable enumDesciption = GetDescriptionContainer();

        private static void AddToEnumDescription(Type enumType)
        {
            enumDesciption.Add(enumType, GetEnumDic(enumType));
        }

        private static string GetDescription(Type enumType, string enumText)
        {
            if (string.IsNullOrEmpty(enumText))
            {
                return null;
            }
            if (!enumDesciption.ContainsKey(enumType))
            {
                AddToEnumDescription(enumType);
            }
            object obj2 = enumDesciption[enumType];
            if ((obj2 == null) || string.IsNullOrEmpty(enumText))
            {
                throw new ApplicationException("不存在枚举的描述");
            }
            Dictionary<string, string> dictionary = (Dictionary<string, string>)obj2;
            return dictionary[enumText];
        }

        private static Hashtable GetDescriptionContainer()
        {
            enumDesciption = new Hashtable();
            return enumDesciption;
        }

        private static Dictionary<string, string> GetEnumDic(Type enumType)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (FieldInfo info in enumType.GetFields())
            {
                if (info.FieldType.IsEnum)
                {
                    object[] customAttributes = info.GetCustomAttributes(typeof(DescriptionAttribute), false);
                    dictionary.Add(info.Name, ((DescriptionAttribute)customAttributes[0]).Description);
                }
            }
            return dictionary;
        }

        public static string ToDescription(this Enum value)
        {
            if (value == null)
            {
                return "";
            }
            Type enumType = value.GetType();
            string name = Enum.GetName(enumType, value);
            return GetDescription(enumType, name);
        }

        public static Dictionary<int, string> ToDictionary<TEnum>()
        {
            Type enumType = typeof(TEnum);
            return toDictionary(enumType);

        }

        private static Dictionary<int, string> toDictionary(Type enumType)
        {
            Array values = Enum.GetValues(enumType);
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            foreach (Enum enum2 in values)
            {
                dictionary.Add(Convert.ToInt32(enum2), enum2.ToDescription());
            }
            return dictionary;
        }

        public static Dictionary<int, string> ToDictionary(string className, string namespaces)
        {
            string typeName = string.Format("{0}, {1}", className, namespaces);
            Type enumType = Type.GetType(typeName);
            return toDictionary(enumType);
        }

        public static SelectList ToSelectList<TEnum>(this TEnum enumObj, bool perfix = true)
        {
            var items = (from e in Enum.GetValues(typeof(TEnum)).Cast<TEnum>() select new { Id = Convert.ToInt32(e), Name = GetDescription(typeof(TEnum), e.ToString()) }).ToList();
            var item = new
            {
                Id = 0,
                Name = "请选择..."
            };
            if (perfix)
            {
                items.Insert(0, item);
            }
            return new SelectList(items, "Id", "Name", enumObj);
        }
    }
}

