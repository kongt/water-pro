
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/11/2017 17:54:52
-- Generated from EDMX file: E:\kongt\net\Kongt.EMO\emo\Kongt.Entity\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [emo];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__ChangeMet__Custo__6477ECF3]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChangeMeterInfo] DROP CONSTRAINT [FK__ChangeMet__Custo__6477ECF3];
GO
IF OBJECT_ID(N'[dbo].[FK__ChangeMet__Opera__656C112C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChangeMeterInfo] DROP CONSTRAINT [FK__ChangeMet__Opera__656C112C];
GO
IF OBJECT_ID(N'[dbo].[FK__ChargeInf__Custo__6A30C649]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ChargeInfo] DROP CONSTRAINT [FK__ChargeInf__Custo__6A30C649];
GO
IF OBJECT_ID(N'[dbo].[FK__Customer__Parent__30F848ED]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK__Customer__Parent__30F848ED];
GO
IF OBJECT_ID(N'[dbo].[FK__Customer__ReadPe__300424B4]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK__Customer__ReadPe__300424B4];
GO
IF OBJECT_ID(N'[dbo].[FK__ExtendCha__Water__15502E78]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ExtendCharge] DROP CONSTRAINT [FK__ExtendCha__Water__15502E78];
GO
IF OBJECT_ID(N'[dbo].[FK__LadderFee__Water__2B3F6F97]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LadderFee] DROP CONSTRAINT [FK__LadderFee__Water__2B3F6F97];
GO
IF OBJECT_ID(N'[dbo].[FK__LateFee__WaterTy__267ABA7A]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LateFee] DROP CONSTRAINT [FK__LateFee__WaterTy__267ABA7A];
GO
IF OBJECT_ID(N'[dbo].[FK__MeterInfo__Custo__46E78A0C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MeterInfo] DROP CONSTRAINT [FK__MeterInfo__Custo__46E78A0C];
GO
IF OBJECT_ID(N'[dbo].[FK__OpenAccou__Custo__3B75D760]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OpenAccountInfo] DROP CONSTRAINT [FK__OpenAccou__Custo__3B75D760];
GO
IF OBJECT_ID(N'[dbo].[FK__OpenAccou__OpenA__412EB0B6]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OpenAccountDocument] DROP CONSTRAINT [FK__OpenAccou__OpenA__412EB0B6];
GO
IF OBJECT_ID(N'[dbo].[FK__OpenAccou__Opera__3C69FB99]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OpenAccountInfo] DROP CONSTRAINT [FK__OpenAccou__Opera__3C69FB99];
GO
IF OBJECT_ID(N'[dbo].[FK_Customer_WaterType_ref]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [FK_Customer_WaterType_ref];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Area]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Area];
GO
IF OBJECT_ID(N'[dbo].[ChangeMeterInfo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChangeMeterInfo];
GO
IF OBJECT_ID(N'[dbo].[ChargeInfo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ChargeInfo];
GO
IF OBJECT_ID(N'[dbo].[Customer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Customer];
GO
IF OBJECT_ID(N'[dbo].[Document]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Document];
GO
IF OBJECT_ID(N'[dbo].[EmployeeInfo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EmployeeInfo];
GO
IF OBJECT_ID(N'[dbo].[ExtendCharge]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ExtendCharge];
GO
IF OBJECT_ID(N'[dbo].[LadderFee]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LadderFee];
GO
IF OBJECT_ID(N'[dbo].[LateFee]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LateFee];
GO
IF OBJECT_ID(N'[dbo].[ManagerInfo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ManagerInfo];
GO
IF OBJECT_ID(N'[dbo].[MeterInfo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MeterInfo];
GO
IF OBJECT_ID(N'[dbo].[OpenAccountDocument]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OpenAccountDocument];
GO
IF OBJECT_ID(N'[dbo].[OpenAccountInfo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OpenAccountInfo];
GO
IF OBJECT_ID(N'[dbo].[WaterType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WaterType];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Areas'
CREATE TABLE [dbo].[Areas] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [FirstArea] nvarchar(500)  NOT NULL,
    [SecondArea] nvarchar(500)  NOT NULL,
    [DetailAddress] nvarchar(500)  NULL
);
GO

-- Creating table 'ChangeMeterInfoes'
CREATE TABLE [dbo].[ChangeMeterInfoes] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [CustomerId] bigint  NOT NULL,
    [NewMeterNo] varchar(500)  NOT NULL,
    [Operator] bigint  NOT NULL,
    [ChangeMeterDate] datetime  NOT NULL,
    [MeterStart] float  NOT NULL,
    [ProcesDate] datetime  NULL
);
GO

-- Creating table 'ChargeInfoes'
CREATE TABLE [dbo].[ChargeInfoes] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [CustomId] bigint  NOT NULL,
    [ChargeName] nvarchar(500)  NULL,
    [LastTime] nvarchar(500)  NULL,
    [LastMeterShow] float  NULL,
    [ThisTime] nvarchar(500)  NULL,
    [ThisMeterShow] float  NULL,
    [WaterMuch] float  NULL,
    [WaterPrice] decimal(19,4)  NULL,
    [TotalWaterPrice] decimal(19,4)  NULL,
    [ExtendPrice] decimal(19,4)  NULL,
    [LateFee] decimal(19,4)  NULL,
    [TotalPrice] decimal(19,4)  NULL,
    [State] int  NOT NULL,
    [CollectMoneyType] int  NOT NULL
);
GO

-- Creating table 'Customers'
CREATE TABLE [dbo].[Customers] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [CustomerId] nvarchar(50)  NOT NULL,
    [CustomerName] nvarchar(50)  NOT NULL,
    [PersonId] varchar(50)  NOT NULL,
    [LinkPhone] varchar(50)  NOT NULL,
    [FirstArea] nvarchar(500)  NULL,
    [SecondArea] nvarchar(500)  NULL,
    [DetailAddress] nvarchar(500)  NULL,
    [MeterNo] nvarchar(50)  NOT NULL,
    [MeterStart] float  NOT NULL,
    [CustomerStat] int  NOT NULL,
    [HouseType] int  NOT NULL,
    [WaterTypeId] bigint  NOT NULL,
    [MeterNature] int  NOT NULL,
    [ReadPersonId] bigint  NULL,
    [OpenAccountDate] datetime  NULL,
    [DESCRIPTION] nvarchar(500)  NULL,
    [ParentId] bigint  NULL,
    [CreateDate] datetime  NOT NULL
);
GO

-- Creating table 'Documents'
CREATE TABLE [dbo].[Documents] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [DocumentNo] varchar(500)  NOT NULL,
    [DocumentName] varchar(500)  NOT NULL,
    [DocumentUnit] varchar(500)  NOT NULL,
    [DocumentPrice] decimal(19,4)  NOT NULL
);
GO

-- Creating table 'EmployeeInfoes'
CREATE TABLE [dbo].[EmployeeInfoes] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [EmployeeNo] varchar(50)  NOT NULL,
    [NAME] nvarchar(50)  NOT NULL,
    [Sex] bit  NOT NULL,
    [PersonID] varchar(50)  NULL,
    [FirstArea] nvarchar(500)  NULL,
    [SecondArea] nvarchar(500)  NULL,
    [DetailAddress] nvarchar(500)  NULL,
    [Phone] varchar(50)  NULL,
    [Description] nvarchar(500)  NULL
);
GO

-- Creating table 'ExtendCharges'
CREATE TABLE [dbo].[ExtendCharges] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [WaterTypeId] bigint  NULL,
    [HasMeter] varchar(50)  NULL,
    [ChargeType] int  NOT NULL,
    [ExtendName] varchar(500)  NULL,
    [FirstArea] nvarchar(500)  NULL,
    [SecondArea] nvarchar(500)  NULL,
    [DetailAddress] nvarchar(500)  NULL,
    [ExtendPrice] decimal(19,4)  NOT NULL,
    [DESCRIPTION] nvarchar(500)  NULL
);
GO

-- Creating table 'LadderFees'
CREATE TABLE [dbo].[LadderFees] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [WaterTypeId] bigint  NULL,
    [START] float  NOT NULL,
    [EndTo] float  NOT NULL,
    [Price] decimal(19,4)  NOT NULL,
    [DESCRIPTION] varchar(500)  NULL
);
GO

-- Creating table 'LateFees'
CREATE TABLE [dbo].[LateFees] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [WaterTypeId] bigint  NULL,
    [GetState] bit  NOT NULL,
    [ChargeType] int  NOT NULL,
    [LevelStand] varchar(500)  NULL,
    [TaxRate] float  NOT NULL,
    [StartDayOnMonth] int  NOT NULL,
    [EffectiveDate] datetime  NOT NULL,
    [StartDay] varchar(500)  NULL,
    [EndDay] varchar(500)  NULL
);
GO

-- Creating table 'ManagerInfoes'
CREATE TABLE [dbo].[ManagerInfoes] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [UserName] varchar(50)  NOT NULL,
    [PASSWORD] varchar(50)  NOT NULL,
    [Roles] varchar(500)  NULL,
    [CreateDate] datetime  NOT NULL
);
GO

-- Creating table 'MeterInfoes'
CREATE TABLE [dbo].[MeterInfoes] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [CustomerId] bigint  NOT NULL,
    [LastShow] float  NOT NULL,
    [ThisShow] float  NOT NULL,
    [ThisLoseWater] float  NOT NULL,
    [ReadMeterDate] datetime  NOT NULL,
    [MeterStat] int  NOT NULL
);
GO

-- Creating table 'OpenAccountDocuments'
CREATE TABLE [dbo].[OpenAccountDocuments] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [OpenAccountInfoId] bigint  NOT NULL,
    [DocumentName] nvarchar(50)  NOT NULL,
    [DocumentUnit] nvarchar(50)  NOT NULL,
    [DocumentPrice] decimal(19,4)  NOT NULL,
    [DocumentNum] int  NOT NULL,
    [DocumentDesc] nvarchar(500)  NULL
);
GO

-- Creating table 'OpenAccountInfoes'
CREATE TABLE [dbo].[OpenAccountInfoes] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [CustomerId] bigint  NOT NULL,
    [OpenAccountCharge] decimal(19,4)  NOT NULL,
    [DocumentCharge] decimal(19,4)  NOT NULL,
    [WorkTimeCharge] decimal(19,4)  NOT NULL,
    [OtherCharge] decimal(19,4)  NOT NULL,
    [ShouldGetCharge] decimal(19,4)  NULL,
    [RealyGetCharge] decimal(19,4)  NULL,
    [RestoreCharge] decimal(19,4)  NULL,
    [OperatorId] bigint  NULL,
    [CreateDate] datetime  NOT NULL
);
GO

-- Creating table 'WaterTypes'
CREATE TABLE [dbo].[WaterTypes] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [TypeName] nvarchar(50)  NOT NULL,
    [MeterType] int  NOT NULL,
    [NoMeter] int  NOT NULL,
    [MeterUse] nvarchar(50)  NULL,
    [Waterprice] decimal(19,4)  NOT NULL,
    [Ladder] bit  NOT NULL,
    [Description] nvarchar(500)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Areas'
ALTER TABLE [dbo].[Areas]
ADD CONSTRAINT [PK_Areas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ChangeMeterInfoes'
ALTER TABLE [dbo].[ChangeMeterInfoes]
ADD CONSTRAINT [PK_ChangeMeterInfoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ChargeInfoes'
ALTER TABLE [dbo].[ChargeInfoes]
ADD CONSTRAINT [PK_ChargeInfoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [PK_Customers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Documents'
ALTER TABLE [dbo].[Documents]
ADD CONSTRAINT [PK_Documents]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmployeeInfoes'
ALTER TABLE [dbo].[EmployeeInfoes]
ADD CONSTRAINT [PK_EmployeeInfoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExtendCharges'
ALTER TABLE [dbo].[ExtendCharges]
ADD CONSTRAINT [PK_ExtendCharges]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LadderFees'
ALTER TABLE [dbo].[LadderFees]
ADD CONSTRAINT [PK_LadderFees]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LateFees'
ALTER TABLE [dbo].[LateFees]
ADD CONSTRAINT [PK_LateFees]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ManagerInfoes'
ALTER TABLE [dbo].[ManagerInfoes]
ADD CONSTRAINT [PK_ManagerInfoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MeterInfoes'
ALTER TABLE [dbo].[MeterInfoes]
ADD CONSTRAINT [PK_MeterInfoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OpenAccountDocuments'
ALTER TABLE [dbo].[OpenAccountDocuments]
ADD CONSTRAINT [PK_OpenAccountDocuments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OpenAccountInfoes'
ALTER TABLE [dbo].[OpenAccountInfoes]
ADD CONSTRAINT [PK_OpenAccountInfoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'WaterTypes'
ALTER TABLE [dbo].[WaterTypes]
ADD CONSTRAINT [PK_WaterTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CustomerId] in table 'ChangeMeterInfoes'
ALTER TABLE [dbo].[ChangeMeterInfoes]
ADD CONSTRAINT [FK__ChangeMet__Custo__6477ECF3]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ChangeMet__Custo__6477ECF3'
CREATE INDEX [IX_FK__ChangeMet__Custo__6477ECF3]
ON [dbo].[ChangeMeterInfoes]
    ([CustomerId]);
GO

-- Creating foreign key on [Operator] in table 'ChangeMeterInfoes'
ALTER TABLE [dbo].[ChangeMeterInfoes]
ADD CONSTRAINT [FK__ChangeMet__Opera__656C112C]
    FOREIGN KEY ([Operator])
    REFERENCES [dbo].[ManagerInfoes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ChangeMet__Opera__656C112C'
CREATE INDEX [IX_FK__ChangeMet__Opera__656C112C]
ON [dbo].[ChangeMeterInfoes]
    ([Operator]);
GO

-- Creating foreign key on [CustomId] in table 'ChargeInfoes'
ALTER TABLE [dbo].[ChargeInfoes]
ADD CONSTRAINT [FK__ChargeInf__Custo__6A30C649]
    FOREIGN KEY ([CustomId])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ChargeInf__Custo__6A30C649'
CREATE INDEX [IX_FK__ChargeInf__Custo__6A30C649]
ON [dbo].[ChargeInfoes]
    ([CustomId]);
GO

-- Creating foreign key on [ParentId] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [FK__Customer__Parent__30F848ED]
    FOREIGN KEY ([ParentId])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Customer__Parent__30F848ED'
CREATE INDEX [IX_FK__Customer__Parent__30F848ED]
ON [dbo].[Customers]
    ([ParentId]);
GO

-- Creating foreign key on [ReadPersonId] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [FK__Customer__ReadPe__300424B4]
    FOREIGN KEY ([ReadPersonId])
    REFERENCES [dbo].[EmployeeInfoes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Customer__ReadPe__300424B4'
CREATE INDEX [IX_FK__Customer__ReadPe__300424B4]
ON [dbo].[Customers]
    ([ReadPersonId]);
GO

-- Creating foreign key on [CustomerId] in table 'MeterInfoes'
ALTER TABLE [dbo].[MeterInfoes]
ADD CONSTRAINT [FK__MeterInfo__Custo__46E78A0C]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__MeterInfo__Custo__46E78A0C'
CREATE INDEX [IX_FK__MeterInfo__Custo__46E78A0C]
ON [dbo].[MeterInfoes]
    ([CustomerId]);
GO

-- Creating foreign key on [CustomerId] in table 'OpenAccountInfoes'
ALTER TABLE [dbo].[OpenAccountInfoes]
ADD CONSTRAINT [FK__OpenAccou__Custo__3B75D760]
    FOREIGN KEY ([CustomerId])
    REFERENCES [dbo].[Customers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__OpenAccou__Custo__3B75D760'
CREATE INDEX [IX_FK__OpenAccou__Custo__3B75D760]
ON [dbo].[OpenAccountInfoes]
    ([CustomerId]);
GO

-- Creating foreign key on [WaterTypeId] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [FK_Customer_WaterType_ref]
    FOREIGN KEY ([WaterTypeId])
    REFERENCES [dbo].[WaterTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Customer_WaterType_ref'
CREATE INDEX [IX_FK_Customer_WaterType_ref]
ON [dbo].[Customers]
    ([WaterTypeId]);
GO

-- Creating foreign key on [WaterTypeId] in table 'ExtendCharges'
ALTER TABLE [dbo].[ExtendCharges]
ADD CONSTRAINT [FK__ExtendCha__Water__15502E78]
    FOREIGN KEY ([WaterTypeId])
    REFERENCES [dbo].[WaterTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__ExtendCha__Water__15502E78'
CREATE INDEX [IX_FK__ExtendCha__Water__15502E78]
ON [dbo].[ExtendCharges]
    ([WaterTypeId]);
GO

-- Creating foreign key on [WaterTypeId] in table 'LadderFees'
ALTER TABLE [dbo].[LadderFees]
ADD CONSTRAINT [FK__LadderFee__Water__2B3F6F97]
    FOREIGN KEY ([WaterTypeId])
    REFERENCES [dbo].[WaterTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__LadderFee__Water__2B3F6F97'
CREATE INDEX [IX_FK__LadderFee__Water__2B3F6F97]
ON [dbo].[LadderFees]
    ([WaterTypeId]);
GO

-- Creating foreign key on [WaterTypeId] in table 'LateFees'
ALTER TABLE [dbo].[LateFees]
ADD CONSTRAINT [FK__LateFee__WaterTy__267ABA7A]
    FOREIGN KEY ([WaterTypeId])
    REFERENCES [dbo].[WaterTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__LateFee__WaterTy__267ABA7A'
CREATE INDEX [IX_FK__LateFee__WaterTy__267ABA7A]
ON [dbo].[LateFees]
    ([WaterTypeId]);
GO

-- Creating foreign key on [OperatorId] in table 'OpenAccountInfoes'
ALTER TABLE [dbo].[OpenAccountInfoes]
ADD CONSTRAINT [FK__OpenAccou__Opera__3C69FB99]
    FOREIGN KEY ([OperatorId])
    REFERENCES [dbo].[ManagerInfoes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__OpenAccou__Opera__3C69FB99'
CREATE INDEX [IX_FK__OpenAccou__Opera__3C69FB99]
ON [dbo].[OpenAccountInfoes]
    ([OperatorId]);
GO

-- Creating foreign key on [OpenAccountInfoId] in table 'OpenAccountDocuments'
ALTER TABLE [dbo].[OpenAccountDocuments]
ADD CONSTRAINT [FK__OpenAccou__OpenA__412EB0B6]
    FOREIGN KEY ([OpenAccountInfoId])
    REFERENCES [dbo].[OpenAccountInfoes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__OpenAccou__OpenA__412EB0B6'
CREATE INDEX [IX_FK__OpenAccou__OpenA__412EB0B6]
ON [dbo].[OpenAccountDocuments]
    ([OpenAccountInfoId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------