﻿namespace Kongt.Entity
{
    using Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    public partial class Entities : DbContext
    {
        private Entities()
            : base("name=dbConstr")
        {
            // this.Configuration.LazyLoadingEnabled = false;
        }

        private static Entities dbContext = null;
        public static Entities getDbContext()
        {
            if (dbContext == null) { dbContext = new Entities(); }
            return dbContext;
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
        public DbSet<Area> Areas { get; set; }
        public DbSet<ChangeMeterInfo> ChangeMeterInfoes { get; set; }
        public DbSet<ChargeInfo> ChargeInfoes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<EmployeeInfo> EmployeeInfoes { get; set; }
        public DbSet<ExtendCharge> ExtendCharges { get; set; }
        public DbSet<LadderFee> LadderFees { get; set; }
        public DbSet<LateFee> LateFees { get; set; }
        public DbSet<ManagerInfo> ManagerInfoes { get; set; }
        public DbSet<MeterInfo> MeterInfoes { get; set; }
        public DbSet<OpenAccountDocument> OpenAccountDocuments { get; set; }
        public DbSet<OpenAccountInfo> OpenAccountInfoes { get; set; }
        public DbSet<WaterType> WaterTypes { get; set; }

        public DbSet<BuyWaterInfo> BuyWaterInfoes { get; set; }

        public DbSet<Freeset> Freesets { get; set; }
    }

}
