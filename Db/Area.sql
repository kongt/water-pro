﻿--基本设置管理 创建区域表
CREATE TABLE [dbo].[Area]
(
	[Id] BIGINT NOT NULL PRIMARY KEY identity(1,1), 
    [FirstArea] NVARCHAR(500) NOT NULL, 
    [SecondArea] NVARCHAR(500) NOT NULL, 
    [DetailAddress] NVARCHAR(500) NULL
)
