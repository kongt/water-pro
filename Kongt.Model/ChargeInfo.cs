
namespace Kongt.Model
{
    using System;
    using System.Collections.Generic;

    public partial class ChargeInfo
    {
        public long Id { get; set; }
        public long CustomId { get; set; }
        public string ChargeName { get; set; }
        public string LastTime { get; set; }
        public Nullable<double> LastMeterShow { get; set; }
        public string ThisTime { get; set; }
        public Nullable<double> ThisMeterShow { get; set; }
        public Nullable<double> WaterMuch { get; set; }
        public Nullable<decimal> WaterPrice { get; set; }
        public Nullable<decimal> TotalWaterPrice { get; set; }
        public Nullable<decimal> ExtendPrice { get; set; }
        public Nullable<decimal> LateFee { get; set; }
        public Nullable<decimal> TotalPrice { get; set; }
        public int State { get; set; }
        public int CollectMoneyType { get; set; }
        public long? Collector { get; set; }
        public DateTime? ColectMoneyDate { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual ManagerInfo ManagerInfo { get; set; }
    }
}
