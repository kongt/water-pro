﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Kongt.Model.Custom
{


    /// <summary>
    /// 汇总统计
    /// </summary>
    public class TotalReportModel
    {

        /// <summary>
        /// 水费金额
        /// </summary>
        public decimal TotalWaterPrice { get; set; }

        /// <summary>
        /// 待收金额
        /// </summary>
        public decimal AmountBeReceived { get; set; }

        /// <summary>
        /// 滞纳金
        /// </summary>
        public decimal TotalLateFee { get; set; }


        /// <summary>
        /// 水费欠收
        /// </summary>
        public decimal WaterPriceBeReceived { get; set; }

        /// <summary>
        /// 用水总数
        /// </summary>
        public float WaterMuch { get; set; }

        /// <summary>
        /// 待收水方数
        /// </summary>
        public float WaterMuchBeReceived { get; set; }

        /// <summary>
        /// 总户数
        /// </summary>
        public int TotalCustomerCount { get; set; }

        /// <summary>
        /// 待收户数
        /// </summary>
        public int TotalCustomerCountBeReceived { get; set; }

        /// <summary>
        /// 实收合计
        /// </summary>
        public decimal HasColectTotal { get; set; }

        /// <summary>
        /// 当前查询条件的账单数据
        /// </summary>
        public  PageModel<ChargeInfo> PageModel { get; set; }

    }
}
