﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kongt.Model
{
    public class PageModel<T>
    {
        public List<T> Rows { get; set; }
        public int Count { get; set; }

        public int GetPCount(int pageSize)
        {
            var p = Count / pageSize;
            return Count % pageSize > 0 ? (++p) : p;
        }
    }
}
