namespace Kongt.Model
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class MeterInfo
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public double LastShow { get; set; }
        public double ThisShow { get; set; }
        public double ThisLoseWater { get; set; }
        public System.DateTime ReadMeterDate { get; set; }
        public int MeterStat { get; set; }

        [JsonIgnore]
        public virtual Customer Customer { get; set; }

        [NotMapped]
        public Dictionary<string, object> CustomerMap
        {
            get
            {
                return this.getCustomerMap();
            }
        }
        private Dictionary<string, object> getCustomerMap()
        {
            Dictionary<string, object> dataMap = new Dictionary<string, object>();
            dataMap.Add("CustomerName", this.Customer.CustomerName);
            dataMap.Add("CustomerId", this.Customer.CustomerId);
            dataMap.Add("FirstArea", this.Customer.FirstArea);
            dataMap.Add("SecondArea", this.Customer.SecondArea);
            dataMap.Add("DetailAddress", this.Customer.DetailAddress);
            dataMap.Add("WaterTypeId", this.Customer.WaterTypeId);
            dataMap.Add("WaterTypeName", this.Customer.WaterType?.TypeName);
            return dataMap;
        }


    }
}
