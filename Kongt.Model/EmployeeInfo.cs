 
namespace Kongt.Model
{
    using System;
    using System.Collections.Generic;

    public partial class EmployeeInfo
    {
        public EmployeeInfo()
        {
            this.Customers = new HashSet<Customer>(); 
        }

        public long Id { get; set; }
        public string EmployeeNo { get; set; }
        public string NAME { get; set; }
        public bool Sex { get; set; }
        public string PersonID { get; set; }
        public string FirstArea { get; set; }
        public string SecondArea { get; set; }
        public string DetailAddress { get; set; }
        public string Phone { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
         
    }
}
