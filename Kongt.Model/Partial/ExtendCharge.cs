﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Kongt.Model
{
    public partial class ExtendCharge
    {
        /// <summary>
        /// 计费方式
        /// </summary>
        public enum ChargeTypeEnum
        {
            [Description("按用量")]
            YongLiang,
            [Description("按固定金额")]
            GuDingJinE,
        }

    }
}
