
namespace Kongt.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public partial class Customer
    {
        /// <summary>
        /// 用户状态
        /// </summary>
        public enum CustomStatEnum
        {
            [Description("正常")]
            ZhengChang,
            [Description("报停")]
            BaoTing,
            [Description("欠费")]
            QianFee,
            [Description("销户")]
            XiaoHu,
        }


        /// <summary>
        /// 水表性质
        /// </summary>
        public enum MeterNatureEnum
        {
            [Description("单户")]
            DanHu,
            [Description("无表")]
            WuBiao,
            [Description("总表")]
            ZongBiao,
        }



        /// <summary>
        /// 房屋类型
        /// </summary>
        public enum HouseTypeEnum
        {
            [Description("楼房")]
            LouFang,
            [Description("平房")]
            PingDang,
            [Description("商铺")]
            ShangPu,
            [Description("洗车洗浴")]
            XiCheXiYu,

        }
    }
}
