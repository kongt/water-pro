
namespace Kongt.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public partial class LateFee
    {

        /// <summary>
        /// 开启状态
        /// </summary>
        public enum GetStatEnum
        {
            [Description("关闭")]
            Off,
            [Description("开启")]
            ON,

        }


        /// <summary>
        /// 计费方式
        /// </summary>
        public enum ChargeTypeEnum
        {
            [Description("固定金额")]
            GuDingJinE,
            [Description("利率")]
            LiLv, 
        }
    }
}
