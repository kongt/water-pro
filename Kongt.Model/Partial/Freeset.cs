namespace Kongt.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    public partial class Freeset
    {
        /// <summary>
        /// 减免方式
        /// </summary>
        public enum FreeMethodEnum
        {
            [Description("全部减免")]
            All,
            [Description("部分减免")]
            Part,
        }



        /// <summary>
        /// 减免期限
        /// </summary>
        public enum FreeLimitEnum
        {
            [Description("永久")]
            ForEver,
            [Description("按次数")]
            ByTimes,
        }



        /// <summary>
        /// 减免类型
        /// </summary>
        public enum DefaultFreeTypeEnum
        {
            [Description("按照金额减免")]
            ByMoney,
            [Description("按次数")]
            ByTimes,
        }
    }
}
