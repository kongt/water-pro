﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Kongt.Model
{
    public partial class ChargeInfo
    {
          
        /// <summary>
        /// 缴费状态
        /// </summary>
        public enum StateEnum
        {
            [Description("未缴费")]
            No,
            [Description("已缴费")]
            Yes,
            [Description("作废")]
            Cancel,
        }


        /// <summary>
        /// 收费方式
        /// </summary>
        public enum CollectMoneyTypeEnum
        {
            [Description("窗口收费")]
            WINDOWS,
            [Description("无表收费")]
            NOMETER,
            [Description("磁卡收费")]
            ECARD,
        }




    }
}
