﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Kongt.Model
{
    public partial class WaterType
    {
        /// <summary>
        /// 有表类型
        /// </summary>
        public enum MeterTypeEnum
        {
            [Description("无")]
            No,
            [Description("云鼎")]
            YunDing,
            [Description("思达")]
            SiDa,
            [Description("普通")]
            PuTong,
            [Description("远达")]
            YuanDa
        }


        /// <summary>
        /// 有表类型
        /// </summary>
        public enum NoMeterEnum
        {
            [Description("无")]
            No,
            [Description("平房")]
            PinFang,
            [Description("楼房")]
            LouFang,
            [Description("商铺")]
            ShangPu,
            [Description("汽车洗浴")]
            QiCheXiYu
        }


    }
}
