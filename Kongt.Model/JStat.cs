﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kongt.Model
{

    [Serializable]
    public class JStat
    {
        public  JStat() { }
        public JStat(object obj) { this.Obj = obj; }
        public bool Succeed { get; set; } = true;
        public string Msg { get; set; } = "成功";

        public object Obj { get; set; }
    }
}
