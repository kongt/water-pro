﻿namespace Kongt.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;

    public class PageModel<T>
    {
        public IEnumerable<T> Rows { get; set; }

        public int Count { get; set; }


        public int GetPcount(int pageSize)
        {
            var p = Count / pageSize;
            return Count % pageSize > 0 ? (++p) : p;
        }
    }


}

