//------------------------------------------------------------------------------
// <auto-generated>
//    此代码是根据模板生成的。
//
//    手动更改此文件可能会导致应用程序中发生异常行为。
//    如果重新生成代码，则将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kongt.Model
{
    using System;
    using System.Collections.Generic;

    public partial class ManagerInfo
    {
        public ManagerInfo()
        {
            this.ChangeMeterInfoes = new HashSet<ChangeMeterInfo>();
            this.OpenAccountInfoes = new HashSet<OpenAccountInfo>();
            this.ChargeInfoes = new HashSet<ChargeInfo>();
        }

        public long Id { get; set; }
        public string UserName { get; set; }
        public string PASSWORD { get; set; }
        public string Roles { get; set; }
        public System.DateTime CreateDate { get; set; }

        public virtual ICollection<ChangeMeterInfo> ChangeMeterInfoes { get; set; }
        public virtual ICollection<OpenAccountInfo> OpenAccountInfoes { get; set; }

        public virtual ICollection<ChargeInfo> ChargeInfoes { get; set; }
    }
}
