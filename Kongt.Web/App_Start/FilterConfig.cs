﻿using System.Web;
using System.Web.Mvc;

namespace Kongt.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //所有回话需要认证
            filters.Add(new AuthorizeAttribute());
        }
    }
}
