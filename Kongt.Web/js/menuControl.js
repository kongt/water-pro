﻿;(function ($) {
    //菜单控制 
    //顶部导航切换
    $(".nav li a").click(function () {
        menuChange(this);
    })

})(jQuery);


function menuChange(a) {
    $(a).parents('.nav').find('a.selected').removeClass("selected");
    $(a).addClass("selected");
    var activeIndex = $(a).parents('li').attr('data-index');
    $(window.parent.frames['leftFrame'].document).contents().find('.leftmenu').children().eq(activeIndex).find('.title').click();
}