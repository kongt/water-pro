﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kongt.Web.Models
{
    public class LoginModel
    {
        [NotMapped]
        public long UserId { get; set; }

        [NotMapped]
        public List<string> Roles { get; set; }

        [Required(ErrorMessage = "请输入用户名")]
        [StringLength(12, ErrorMessage = "用户名长度5至12位", MinimumLength = 5)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "请输入密码")]
        [MinLength(6, ErrorMessage = "密码长度至少6位")]
        public string Password { get; set; }
        public bool RememberPassword { get; set; }

    }
}