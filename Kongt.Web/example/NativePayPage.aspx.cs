﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Text;
using ThoughtWorks;
using ThoughtWorks.QRCode;
using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;
using Kongt.ServiceProvider;
using Kongt.IServices;
using System.Text.RegularExpressions;

namespace WxPayAPI
{
    public partial class NativePayPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //根据id获得支付条目信息发起支付
            Log.Info(this.GetType().ToString(), "page load");
            var payId=  this.Request["payId"];
            var  orderId = 0L;
            try
            {
                orderId = long.Parse(payId);
            }
            catch (Exception)
            {
                Response.Write("传入的单据号有问题,无法发起支付");
                Response.Flush();
                Response.End();
            }
         
           var buyWater= ServiceFactory.Create<IBuyWaterService>().Find(orderId);


            NativePay nativePay = new NativePay();

            //生成扫码支付模式一url
        //    string url1 = nativePay.GetPrePayUrl("cf123456");

            //生成扫码支付模式二url
            string url2 = nativePay.GetPayUrl(orderId.ToString(),(int)(buyWater.RechargeMoney * 100), "买水"+buyWater.BuyWater);

            //将url生成二维码图片
          //  Image1.ImageUrl = "/example/MakeQRCode.aspx?data=" + HttpUtility.UrlEncode(url1);
            Image2.ImageUrl = "/example/MakeQRCode.aspx?data=" + HttpUtility.UrlEncode(url2);
        }
    }
}