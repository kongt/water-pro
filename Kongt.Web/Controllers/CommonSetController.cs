﻿
using Kongt.IServices;
using Kongt.Model;
using Kongt.ServiceProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;

namespace Kongt.Web.Controllers
{

    /// <summary>
    /// 基本设置
    /// </summary>
    public class CommonSetController : BaseUserController
    {
        ICommonSetService commonservice = ServiceFactory.Create<ICommonSetService>();



        /// <summary>
        /// 居住区域设置
        /// </summary>
        public ActionResult AreaSet(QueryBase p)
        {
            var pModel = commonservice.QueryArea(p);
            PagedList<Area> model = new PagedList<Area>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }


        /// <summary>
        /// 增加修改
        /// </summary>
        /// <param name="area"></param>
        [HttpPost]
        public JsonResult AreaEdit(Area area)
        {
            if (ModelState.IsValid)
            {
                if (area.Id > 0)
                {
                    commonservice.UpdateArea(area);
                }
                else
                {
                    commonservice.CreateArea(area);
                }
                return Json(new JStat());
            }
            else
            {
                return Json(new JStat() { Succeed = false, Msg = ModelState.Values.First().Errors[0].ErrorMessage });
            }
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="area"></param>   
        [HttpPost]
        public JsonResult AreaDel(long areaId)
        {
            commonservice.DeleteArea(areaId);
            return Json(new JStat());
        }



        /// <summary>
        /// 用水类别设置
        /// </summary>
        public ActionResult WaterTypeSet(QueryBase p)
        {
            var pModel = commonservice.QueryWaterType(p);
            PagedList<WaterType> model = new PagedList<WaterType>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }


        /// <summary>
        /// 增加修改
        /// </summary>
        /// <param name="area"></param>
        [HttpPost]
        public JsonResult WaterTypeEdit(WaterType type)
        {
            if (ModelState.IsValid)
            {
                if (type.Id > 0)
                {
                    commonservice.UpdateWaterType(type);
                }
                else
                {
                    commonservice.CreateWaterType(type);
                }
                return Json(new JStat());
            }
            else
            {
                return Json(new JStat() { Succeed = false, Msg = ModelState.Values.First().Errors[0].ErrorMessage });
            }
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="area"></param>
        [HttpPost]
        public JsonResult WaterTypeDel(long typeId)
        {
            commonservice.DeleteWaterType(typeId);
            return Json(new JStat());
        }


        /// <summary>
        /// 抄表人员设置
        /// </summary>
        public ActionResult ReadMeterset(QueryBase p)
        {
            var pModel = commonservice.QueryEmployee(p);
            PagedList<EmployeeInfo> model = new PagedList<EmployeeInfo>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }



        /// <summary>
        /// 增加修改
        /// </summary>
        /// <param name="area"></param>
        [HttpPost]
        public JsonResult ReadMeterEdit(EmployeeInfo employee)
        {
            if (ModelState.IsValid)
            {
                if (employee.Id > 0)
                {
                    commonservice.UpdateEmployee(employee);
                }
                else
                {
                    commonservice.CreateEmployee(employee);
                }
                return Json(new JStat());
            }
            else
            {

                return Json(new JStat() { Succeed = false, Msg = ModelState.Values.First().Errors[0].ErrorMessage });
            }
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="area"></param>
        [HttpPost]
        public JsonResult ReadMeterDel(long id)
        {
            commonservice.DeleteEmployee(id);
            return Json(new JStat());
        }



        /// <summary>
        /// 附加费设置
        /// </summary>
        public ActionResult ExtendChargeSet(QueryBase p)
        {
            var pModel = commonservice.QueryExtendCharge(p);
            PagedList<ExtendCharge> model = new PagedList<ExtendCharge>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }

        /// <summary>
        /// 增加修改
        /// </summary>
        /// <param name="area"></param>
        [HttpPost]
        public JsonResult ExtendChargeEdit(ExtendCharge charge)
        {
            if (ModelState.IsValid)
            {
                if (charge.Id > 0)
                {
                    commonservice.UpdateExtendCharge(charge);
                }
                else
                {
                    commonservice.CreateExtendCharge(charge);
                }
                return Json(new JStat());
            }
            else
            {
                return Json(new JStat() { Succeed = false, Msg = ModelState.Values.First().Errors[0].ErrorMessage });
            }
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="area"></param>   
        [HttpPost]
        public JsonResult ExtendChargeDel(long id)
        {
            commonservice.DeleteExtendCharge(id);
            return Json(new JStat());
        }







        /// <summary>
        /// 材料名称设置
        /// </summary>
        public ActionResult DocumentNameSet(QueryBase p)
        {
            var pModel = commonservice.QueryDocument(p);
            PagedList<Document> model = new PagedList<Document>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }

        /// <summary>
        /// 增加修改
        /// </summary>
        /// <param name="area"></param>
        [HttpPost]
        public JsonResult DocumentNameEdit(Document doc)
        {
            if (ModelState.IsValid)
            {
                if (doc.Id > 0)
                {
                    commonservice.UpdateDocument(doc);
                }
                else
                {
                    commonservice.CreateDocument(doc);
                }
                return Json(new JStat());
            }
            else
            {
                return Json(new JStat() { Succeed = false, Msg = ModelState.Values.First().Errors[0].ErrorMessage });
            }
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="area"></param>   
        [HttpPost]
        public JsonResult DocumentNameDel(long id)
        {
            commonservice.DeleteDocument(id);
            return Json(new JStat());
        }






        /// <summary>
        /// 滞纳金设置
        /// </summary>
        public ActionResult LateFeeSet(QueryBase p)
        {
            var pModel = commonservice.QueryLateFee(p);
            PagedList<LateFee> model = new PagedList<LateFee>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }

        /// <summary>
        /// 增加修改
        /// </summary>
        /// <param name="area"></param>
        [HttpPost]
        public JsonResult LateFeeEdit(LateFee fee)
        {
            if (ModelState.IsValid)
            {
                if (fee.Id > 0)
                {
                    commonservice.UpdateLateFee(fee);
                }
                else
                {
                    commonservice.CreateLateFee(fee);
                }
                return Json(new JStat());
            }
            else
            {
                return Json(new JStat() { Succeed = false, Msg = ModelState.Values.First().Errors[0].ErrorMessage });
            }
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="area"></param>   
        [HttpPost]
        public JsonResult LateFeeDel(long id)
        {
            commonservice.DeleteLateFee(id);
            return Json(new JStat());
        }

        /// <summary>
        /// 阶梯水价设置
        /// </summary>
        public ActionResult LadderFeeSet(QueryBase p)
        {
            var pModel = commonservice.QueryLadderFee(p);
            PagedList<LadderFee> model = new PagedList<LadderFee>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }


        /// <summary>
        /// 增加修改
        /// </summary>
        /// <param name="area"></param>
        [HttpPost]
        public JsonResult LadderFeeEdit(LadderFee fee)
        {
            if (ModelState.IsValid)
            {
                if (fee.Id > 0)
                {
                    commonservice.UpdateLadderFee(fee);
                }
                else
                {
                    commonservice.CreateLadderFee(fee);
                }
                return Json(new JStat());
            }
            else
            {
                return Json(new JStat() { Succeed = false, Msg = ModelState.Values.First().Errors[0].ErrorMessage });
            }
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="area"></param>   
        [HttpPost]
        public JsonResult LadderFeeDel(long id)
        {
            commonservice.DeleteLadderFee(id);
            return Json(new JStat());
        }



        /// <summary>
        /// 减免设置
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public ActionResult FreeSet(QueryBase p)
        {
            var pModel = commonservice.QueryFreeSet(p);
            PagedList<Freeset> model = new PagedList<Freeset>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }


        /// <summary>
        /// 减免编辑
        /// </summary>
        /// <param name="fee"></param>
        /// <returns></returns>
        public ActionResult FreeEdit(Freeset fee)
        {
            if (ModelState.IsValid)
            {
                if (fee.Id > 0)
                {
                    commonservice.UpdateFreeSet(fee);
                }
                else
                {
                    commonservice.CreateFreeSet(fee);
                }
                return RedirectToAction("FreeSet");
            }
            else
            {
                return Json(new JStat() { Succeed = false, Msg = ModelState.Values.First().Errors[0].ErrorMessage });
            }
        }

        /// <summary>
        /// 减免删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult FreeDel(long id)
        {
            commonservice.DeleteFreeSet(id);
            return Json(new JStat());
        }














    }
}