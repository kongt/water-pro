﻿using Kongt.IServices;
using Kongt.Model;
using Kongt.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kongt.Web.Controllers
{
    /// <summary>
    /// 系统设置
    /// </summary>
    public class SysSetController : BaseUserController
    {
        public ActionResult test()
        {
            ServiceProvider.ServiceFactory.Create<ICommonSetService>().CreateArea(new Area()
            {
                FirstArea = "北京",
                SecondArea = "通州",
                DetailAddress = "南正街"
            });
            return Content("111");
        }
    }
}