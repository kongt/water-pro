﻿using Kongt.IServices;
using Kongt.IServices.QueryModel;
using Kongt.Model;
using Kongt.ServiceProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;

namespace Kongt.Web.Controllers
{
    /// <summary>
    /// 收费
    /// </summary>
    public class ChargeController : BaseUserController
    {


        /// <summary>
        /// 窗口收费
        /// </summary>
        public ActionResult WindowCharge(ChargeQuery p)
        {
            ViewBag.p = p;
            if (p.CustomId == null) { return View(); }
            var customer = ServiceFactory.Create<ICustomService>().Find(p.CustomId.Value);
            ViewBag.Customer = customer;
            var pModel = ServiceProvider.ServiceFactory.Create<IChargeService>().QueryChargeInfoes(p);
            PagedList<ChargeInfo> model = new PagedList<ChargeInfo>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }


        /// <summary>
        /// 更新账单状态
        /// </summary>
        /// <returns></returns>
        public ActionResult UpdateChargeStat(ChargeInfo.StateEnum stat, ChargeInfo.CollectMoneyTypeEnum? colectType, String ids)
        {
            var Ids = ids.Split(',').Select(c => long.Parse(c)).ToArray();
            ServiceFactory.Create<IChargeService>().UpdateChargeInfos(stat, colectType, CurrentUser.UserId, Ids);
            return Json(new JStat());
        }



        /// <summary>
        /// 无表收费
        /// </summary>
        public ActionResult NoMeterCharge(ChargeQuery p)
        {
            return WindowCharge(p);
        }

        /// <summary>
        /// 磁卡收费
        /// </summary>
        public ActionResult CardCharge(ChargeQuery p) { return WindowCharge(p); }


        /// <summary>
        /// 已收费查询
        /// </summary>
        public ActionResult ChargedSearch(ChargeQuery p)
        {
            ViewBag.p = p;
            var pModel = ServiceFactory.Create<IChargeService>().QueryChargeInfoes(p);
            PagedList<ChargeInfo> model = new PagedList<ChargeInfo>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }


        public ActionResult Card4442(ChargeQuery p)
        {
            ViewBag.p = p;
            var pModel = ServiceFactory.Create<IChargeService>().QueryChargeInfoes(p);
            PagedList<ChargeInfo> model = new PagedList<ChargeInfo>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }

        public ActionResult Card4428(ChargeQuery p)
        {
            ViewBag.p = p;
            var pModel = ServiceFactory.Create<IChargeService>().QueryChargeInfoes(p);
            PagedList<ChargeInfo> model = new PagedList<ChargeInfo>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }


        [HttpPost]
        public ActionResult Recharge(BuyWaterInfo info) {
            info.RechargeDate = DateTime.Now;
           ServiceFactory.Create<IBuyWaterService>().Create(info);
            return Json(new JStat()); 
        }


        [HttpGet][AllowAnonymous]
        public ActionResult GoPay(long payId)
        {
            return Redirect("/example/NativePayPage.aspx?payId="+payId);
        }

    }
}