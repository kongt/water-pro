﻿
using Kongt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Security;
using Kongt.Web.Models;
using Newtonsoft.Json;

namespace Kongt.Web.Controllers
{
    public class BaseController : Controller
    {



        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            filterContext.ExceptionHandled = true;
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = Json(new JStat()
                {
                    Succeed = false,
                    Msg = filterContext.Exception.Message
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                filterContext.Result = Content(filterContext.Exception.ToString());
            }
        }




        /// <summary>
        /// 分页属性
        /// </summary>
        public static PagerOptions PagerOptions
        {
            get
            {
                return new PagerOptions
                {
                    PageIndexParameterName = "PageIndex",
                    NumericPagerItemCount = 10,
                    ContainerTagName = "ul",
                    CssClass = "paginList",
                    CurrentPagerItemTemplate = "<li class=\"paginItem current\"><a href=\"javascript:; \">{0}</a></li>",
                    DisabledPagerItemTemplate = "<li class=\"paginItem disabled\"><a href=\"javascript:?\">{0}</a></li>",
                    PagerItemTemplate = "<li class=\"paginItem\">{0}</li>",
                    NextPageText = "<span class=\"pagenxt\"></span>",
                    PrevPageText = "<span class=\"pagepre\"></span>",
                    FirstPageText = "<span class=\"\">首页</span>",
                    LastPageText = "<span class=\"\">末页</span>",
                    PageIndexOutOfRangeErrorMessage = "页码错误"
                };
            }

        }

    }
}