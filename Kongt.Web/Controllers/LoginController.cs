﻿using Kongt.IServices;
using Kongt.Model;
using Kongt.ServiceProvider;
using Kongt.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Kongt.Web.Controllers
{

    public class LoginController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Json()
        {
            var model = ServiceFactory.Create<ICustomService>().Find(1);
            var str = Newtonsoft.Json.JsonConvert.SerializeObject(model, new Newtonsoft.Json.JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
            return Content(str);

        }



        [AllowAnonymous]
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [ActionName("Index")]
        [HttpPost]
        public ActionResult CheckLogin(LoginModel login, string ReturnUrl)
        {

            if (ModelState.IsValid)
            {
                var entity = ServiceFactory.Create<IManagerService>().GetManagerByUserName(login.UserName);
                if (entity == null)
                {
                    ModelState.AddModelError("UserName", "用户名不存在");
                    return View(login);
                }
                else if (entity.PASSWORD != FormsAuthentication.HashPasswordForStoringInConfigFile(login.Password, "md5"))
                {
                    ModelState.AddModelError("Password", "密码输入错误");
                    return View(login);
                }
                else
                {
                    login.UserId = entity.Id;
                    login.Roles = (string.IsNullOrEmpty(entity.Roles)) ? entity.Roles.Split(',').ToList() : null;
                    var ticket = new FormsAuthenticationTicket(2, login.UserName, DateTime.Now, DateTime.Now.AddDays(1), true, JsonConvert.SerializeObject(login));
                    var cookieValue = FormsAuthentication.Encrypt(ticket);
                    FormsAuthentication.SetAuthCookie(login.UserName, false);
                    var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookieValue);
                    cookie.Expires = DateTime.Now.AddMinutes(30);
                    Response.AppendCookie(cookie);
                    if (!string.IsNullOrEmpty(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(login);

        }



        [AllowAnonymous]
        public ActionResult LoginOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }



        [AllowAnonymous]
        public ActionResult register()
        {
            ServiceFactory.Create<IManagerService>().Create(new Model.ManagerInfo()
            {
                UserName = "admin",
                PASSWORD = FormsAuthentication.HashPasswordForStoringInConfigFile("admin888", "md5"),
                Roles = "admin",
                CreateDate = DateTime.Now
            });
            return Json(new JStat(), JsonRequestBehavior.AllowGet);
        }

    }
}