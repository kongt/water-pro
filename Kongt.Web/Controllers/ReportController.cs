﻿using Kongt.IServices;
using Kongt.IServices.QueryModel;
using Kongt.Model;
using Kongt.Model.Custom;
using Kongt.ServiceProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;

namespace Kongt.Web.Controllers
{

    /// <summary>
    /// 数据统计
    /// </summary>
    public class ReportController : BaseUserController
    {
        ICustomService customService = ServiceFactory.Create<ICustomService>();

        /// <summary>
        /// 用户查询
        /// </summary>
        public ActionResult UserReport(CustomQuery p)
        {
            var pModel = customService.QueryCustomers(p);
            PagedList<Customer> model = new PagedList<Customer>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }

        /// <summary>
        /// 明细查询
        /// </summary>
        public ActionResult DetailReport(ChargeQuery p)
        {
            ViewBag.p = p;
            var pModel = ServiceFactory.Create<IChargeService>().QueryChargeInfoes(p);
            PagedList<ChargeInfo> model = new PagedList<ChargeInfo>(pModel.Rows, p.PageIndex, p.PageSize, pModel.Count);
            return View(model);
        }

        /// <summary>
        /// 汇总查询
        /// </summary>
        public ActionResult TotalReport()
        {
            return View(new TotalReportModel());
        }

        [HttpPost]
        public ActionResult TotalReport(TotalReportQuery p)
        { 
            var reportModel = ServiceFactory.Create<IChargeService>().GetTotalReportModel(p);
            PagedList<ChargeInfo> pmodel = new PagedList<ChargeInfo>(reportModel.PageModel.Rows, p.PageIndex, p.PageSize, reportModel.PageModel.Count);
            ViewBag.pmodel = pmodel;
            ViewBag.p = p;
            return View(reportModel);
        }


        /// <summary>
        /// 欠费统计
        /// </summary>
        public ActionResult DebtReport(ChargeQuery p) { return DetailReport(p); }


    }
}