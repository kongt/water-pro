﻿using Kongt.Common;
using Kongt.IServices;
using Kongt.Model;
using Kongt.ServiceProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kongt.Web.Controllers
{

    [AllowAnonymous]
    public class CommonController : BaseController
    {
        // GET: Common
        public ActionResult _Left()
        {
            return View();
        }


        public ActionResult _Top()
        {
            return View();
        }



        #region 三级区域
        public ActionResult _LinkArea(string firstAreaId = "firstArea", string secondAreaId = "secondArea")
        {
            var list = ServiceFactory.Create<ICommonSetService>().GetFirstArea();
            ViewBag.firstAreaId = firstAreaId;
            ViewBag.secondAreaId = secondAreaId;
            return PartialView(list);
        }

        [HttpPost]
        public ActionResult _GetSecondArea(string firstArea)
        {
            var list = ServiceFactory.Create<ICommonSetService>().GetSecondAreaByFirstArea(firstArea);
            return Json(new JStat() { Obj = list });
        }
        #endregion


        #region 用水类别
        [ChildActionOnly]
        public ActionResult _WaterType(string id = "waterType", string name = "WaterTypeId", string cssClass = "select3")
        {
            ViewBag.id = id;
            ViewBag.name = name;
            ViewBag.cssClass = cssClass;
            var list = ServiceFactory.Create<ICommonSetService>().QueryWaterType(new QueryBase() { PageSize = 10000 });
            return PartialView(list.Rows);
        }
        #endregion



        /// <summary>
        /// 将枚举转化为列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [ChildActionOnly]
        public ActionResult _EnumSelect(string className, string namespaces, string id = "customStat", string name = "customStat", string cssClass = "")
        {
            ViewBag.id = id;
            ViewBag.name = name;
            ViewBag.cssClass = cssClass;
            var customStatDic = EnumHelper.ToDictionary(string.Concat(namespaces, ".", className), namespaces);
            return PartialView(customStatDic);
        }



        [ChildActionOnly]
        public ActionResult _Employees(string id = "ReadPersonId", string name = "ReadPersonId", string cssClass = "select3")
        {
            ViewBag.id = id;
            ViewBag.name = name;
            ViewBag.cssClass = cssClass;
            var employeeModel = ServiceFactory.Create<ICommonSetService>().QueryEmployee(new QueryBase() { PageSize = 10000 });
            return PartialView(employeeModel.Rows);
        }



        [ChildActionOnly]
        public ActionResult _Managers(string id = "manager", string name = "manager", string cssClass = "select3")
        {
            ViewBag.id = id;
            ViewBag.name = name;
            ViewBag.cssClass = cssClass;
            var pmodel = ServiceFactory.Create<IManagerService>().Query(new QueryBase() { PageIndex = 1, PageSize = 10000 });
            return PartialView(pmodel.Rows);
        }

    }
}