﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kongt.Web.Controllers
{
    public class HomeController : BaseUserController
    {

        /// <summary>
        /// 框架首页
        /// </summary> 
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// 框架欢迎页
        /// </summary>
        public ActionResult Home()
        {
            return View();
        }



        /// <summary>
        /// 没有认证
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult NoPower()
        {
            return View();
        }

    }
}