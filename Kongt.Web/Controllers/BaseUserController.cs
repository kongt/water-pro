﻿using Kongt.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Security;

namespace Kongt.Web.Controllers
{
    public class BaseUserController : BaseController
    {
        protected LoginModel CurrentUser
        {
            get
            {
                var cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                var ticket = FormsAuthentication.Decrypt(cookie.Value);
                return JsonConvert.DeserializeObject<LoginModel>(ticket.UserData);
            }
        }



        /// <summary>
        /// 当需要权限时拦截
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
        }


        /// <summary>
        /// 当需要认证时拦截
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            base.OnAuthenticationChallenge(filterContext);

        }
    }
}