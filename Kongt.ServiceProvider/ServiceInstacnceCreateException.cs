﻿using System;
namespace Kongt.ServiceProvider
{
    public class ServiceInstacnceCreateException : Exception
    {
        public ServiceInstacnceCreateException()
        {
        }

        public ServiceInstacnceCreateException(string message) : base(message)
        {
        }

        public ServiceInstacnceCreateException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}

