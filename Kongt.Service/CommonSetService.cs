﻿using Kongt.IServices;
using Kongt.Service;
using System;
using Kongt.Model;
using System.Collections.Generic;
using System.Linq;

namespace Kongt.Service
{
    /// <summary>
    /// 区域
    /// </summary>
    public class CommonSetService : ServiceBase, IService, IDisposable, ICommonSetService
    {

        public void CreateArea(Area area)
        {
            context.Areas.Add(area);
            context.SaveChanges();
        }



        public void DeleteArea(long id)
        {
            context.Areas.Remove(this.FindArea(id));
            context.SaveChanges();
        }



        public Area FindArea(long id)
        {
            return context.Areas.Find(id);
        }



        public List<string> GetSecondAreaByFirstArea(string firstArea)
        {
            return context.Areas.Where(item => item.FirstArea.Contains(firstArea)).Select(c => c.SecondArea).Distinct().ToList();
        }

        public PageModel<Area> QueryArea(QueryBase Query)
        {
            var query = context.Areas.OrderByDescending(item => item.Id);
            return new PageModel<Area>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }



        public void UpdateArea(Area area)
        {
            var entity = context.Areas.Find(area.Id);
            entity.FirstArea = area.FirstArea;
            entity.SecondArea = area.SecondArea;
            entity.DetailAddress = area.DetailAddress;
            context.SaveChanges();
        }

        public void UpdateWaterType(WaterType type)
        {
            var entity = this.FindWaterType(type.Id);
            entity.TypeName = type.TypeName;
            entity.MeterType = type.MeterType;
            entity.NoMeter = type.NoMeter;
            entity.MeterUse = type.MeterUse;
            entity.Waterprice = type.Waterprice;
            entity.Ladder = type.Ladder;
            entity.Description = type.Description;
            context.SaveChanges();
        }
        public void CreateWaterType(WaterType type)
        {
            context.WaterTypes.Add(type);
            context.SaveChanges();
        }
        public void DeleteWaterType(long WaterTypeId)
        {
            context.WaterTypes.Remove(this.FindWaterType(WaterTypeId));
            context.SaveChanges();
        }
        public PageModel<WaterType> QueryWaterType(QueryBase Query)
        {
            var query = context.WaterTypes.OrderByDescending(item => item.Id);
            return new PageModel<WaterType>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }
        public WaterType FindWaterType(long WaterTypeId)
        {
            return context.WaterTypes.Find(WaterTypeId);
        }

        public void CreateEmployee(EmployeeInfo employee)
        {
            context.EmployeeInfoes.Add(employee);
            context.SaveChanges();
        }

        public void UpdateEmployee(EmployeeInfo employee)
        {
            var entity = this.FindEmployee(employee.Id);
            entity.EmployeeNo = employee.EmployeeNo;
            entity.NAME = employee.NAME;
            entity.Sex = employee.Sex;
            entity.PersonID = employee.PersonID;
            entity.FirstArea = employee.FirstArea;
            entity.SecondArea = employee.SecondArea;
            entity.DetailAddress = employee.DetailAddress;
            entity.Phone = employee.Phone;
            entity.Description = employee.Description;
            context.SaveChanges();
        }

        public void DeleteEmployee(long employeeId)
        {
            context.EmployeeInfoes.Remove(this.FindEmployee(employeeId));
            context.SaveChanges();
        }

        public EmployeeInfo FindEmployee(long id)
        {
            return context.EmployeeInfoes.Find(id);
        }

        public PageModel<EmployeeInfo> QueryEmployee(QueryBase Query)
        {
            var query = context.EmployeeInfoes.OrderByDescending(item => item.Id);
            return new PageModel<EmployeeInfo>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }

        public List<string> GetFirstArea()
        {
            return context.Areas.Select(c => c.FirstArea).Distinct().ToList();
        }

        public void CreateExtendCharge(ExtendCharge charge)
        {
            context.ExtendCharges.Add(charge);
            context.SaveChanges();
        }

        public void UpdateExtendCharge(ExtendCharge charge)
        {
            var entity = this.FindExtendCharge(charge.Id);
            entity.WaterTypeId = charge.WaterTypeId;
            entity.HasMeter = charge.HasMeter;
            entity.ChargeType = charge.ChargeType;
            entity.ExtendName = charge.ExtendName;
            entity.FirstArea = charge.FirstArea;
            entity.SecondArea = charge.SecondArea;
            entity.DetailAddress = charge.DetailAddress;
            entity.ExtendPrice = charge.ExtendPrice;
            entity.DESCRIPTION = charge.DESCRIPTION;
            context.SaveChanges();
        }

        public void DeleteExtendCharge(long id)
        {
            context.ExtendCharges.Remove(this.FindExtendCharge(id));
            context.SaveChanges();
        }

        public ExtendCharge FindExtendCharge(long id)
        {
            return context.ExtendCharges.Find(id);
        }

        public PageModel<ExtendCharge> QueryExtendCharge(QueryBase Query)
        {
            var query = context.ExtendCharges.OrderByDescending(item => item.Id);
            return new PageModel<ExtendCharge>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }

        public void CreateDocument(Document doc)
        {
            context.Documents.Add(doc);
            context.SaveChanges();
        }

        public void UpdateDocument(Document doc)
        {
            var entity = this.FindDocument(doc.Id);
            entity.DocumentName = doc.DocumentName;
            entity.DocumentNo = doc.DocumentNo;
            entity.DocumentPrice = doc.DocumentPrice;
            entity.DocumentUnit = doc.DocumentUnit;
            context.SaveChanges();
        }

        public void DeleteDocument(long id)
        {
            context.Documents.Remove(this.FindDocument(id));
            context.SaveChanges();
        }

        public Document FindDocument(long id)
        {
            return context.Documents.Find(id);
        }

        public PageModel<Document> QueryDocument(QueryBase Query)
        {
            var query = context.Documents.OrderByDescending(item => item.Id);
            return new PageModel<Document>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }

        public void CreateLateFee(LateFee fee)
        {
            context.LateFees.Add(fee);
            context.SaveChanges();
        }

        public void UpdateLateFee(LateFee fee)
        {
            var entity = this.FindLateFee(fee.Id);
            entity.WaterTypeId = fee.WaterTypeId;
            entity.GetState = fee.GetState;
            entity.ChargeType = fee.ChargeType;
            entity.LevelStand = fee.LevelStand;
            entity.TaxRate = fee.TaxRate;
            entity.StartDayOnMonth = fee.StartDayOnMonth;
            entity.EffectiveDate = fee.EffectiveDate;
            entity.StartDay = fee.StartDay;
            entity.EndDay = fee.EndDay;
            context.SaveChanges();
        }

        public void DeleteLateFee(long id)
        {
            context.LateFees.Remove(this.FindLateFee(id));
            context.SaveChanges();
        }

        public LateFee FindLateFee(long id)
        {
            return context.LateFees.Find(id);
        }

        public PageModel<LateFee> QueryLateFee(QueryBase Query)
        {
            var query = context.LateFees.OrderByDescending(item => item.Id);
            return new PageModel<LateFee>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }

        public void CreateLadderFee(LadderFee fee)
        {
            context.LadderFees.Add(fee);
            context.SaveChanges();
        }

        public void UpdateLadderFee(LadderFee fee)
        {
            var entity = this.FindLadderFee(fee.Id);
            entity.WaterTypeId = fee.WaterTypeId;
            entity.START = fee.START;
            entity.EndTo = fee.EndTo;
            entity.Price = fee.Price;
            entity.DESCRIPTION = fee.DESCRIPTION;
            context.SaveChanges();
        }

        public void DeleteLadderFee(long id)
        {
            context.LadderFees.Remove(this.FindLadderFee(id));
            context.SaveChanges();
        }

        public LadderFee FindLadderFee(long id)
        {
            return context.LadderFees.Find(id);
        }

        public PageModel<LadderFee> QueryLadderFee(QueryBase Query)
        {
            var query = context.LadderFees.OrderByDescending(item => item.Id);
            return new PageModel<LadderFee>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }





        public void CreateFreeSet(Freeset fee)
        {
            context.Freesets.Add(fee);
            context.SaveChanges();
        }

        public void UpdateFreeSet(Freeset fee)
        {
            var entity = this.FindFreeSet(fee.Id);
            entity.CustomerId = fee.CustomerId;
            entity.FirstArea = fee.FirstArea;
            entity.SecondArea = fee.SecondArea;
            entity.DetailAddress = fee.DetailAddress;
            entity.FreeMethod = fee.FreeMethod;
            entity.FreeMoney = fee.FreeMoney;
            entity.FreeLimit = fee.FreeLimit;
            entity.FreeTimes = fee.FreeTimes;
            entity.FreeMethod1 = fee.FreeMethod1;
            entity.FreeWaterMuch = fee.FreeWaterMuch;
            entity.FreeLimit1 = fee.FreeLimit1;
            entity.FreeTimes1 = fee.FreeTimes1;
            entity.defaultExecuteType = fee.defaultExecuteType;
            context.SaveChanges();
        }

        public void DeleteFreeSet(long id)
        {
            context.Freesets.Remove(this.FindFreeSet(id));
            context.SaveChanges();
        }

        public Freeset FindFreeSet(long id)
        {
            return context.Freesets.Find(id);
        }

        public PageModel<Freeset> QueryFreeSet(QueryBase Query)
        {
            var query = context.Freesets.OrderByDescending(item => item.Id);
            return new PageModel<Freeset>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }





    }
}

