﻿using Kongt.IServices;
using Kongt.Service;
using System;
using Kongt.Model;
using System.Collections.Generic;
using System.Linq;
using Kongt.IServices.QueryModel;
using System.Data.SqlClient;
using Kongt.ServiceProvider;
using static Kongt.Model.LateFee;
using Kongt.Model.Custom;

namespace Kongt.Service
{

    public class BuyWaterService : ServiceBase, IService, IDisposable, IBuyWaterService
    {
        public void Create(BuyWaterInfo ci)
        {
            context.BuyWaterInfoes.Add(ci);
            context.SaveChanges();
        }

        public void Delete(long id)
        {
            context.BuyWaterInfoes.Remove(this.Find(id));
            context.SaveChanges();
        }

        public BuyWaterInfo Find(long id)
        {
          return   context.BuyWaterInfoes.Find(id);
        }

        public PageModel<BuyWaterInfo> QueryBuyWaterInfoes(BuyWaterQuery Query)
        {
            throw new NotImplementedException();
        }
         

        public void Update(BuyWaterInfo ci)
        {
            var buyWaterInfo=   this.Find(ci.Id);
            buyWaterInfo.CardNo = ci.CardNo;
            buyWaterInfo.BuyWater = ci.BuyWater;
            buyWaterInfo.UseWater = ci.UseWater;
            buyWaterInfo.LeftWater = ci.LeftWater;
            buyWaterInfo.RechargeStat = ci.RechargeStat;
            buyWaterInfo.RechargeMoney = ci.RechargeMoney;
            buyWaterInfo.RechargeDate = ci.RechargeDate;
            buyWaterInfo.PayMethod = ci.PayMethod;
            context.SaveChanges();
        }
    }
}