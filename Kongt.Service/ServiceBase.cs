﻿using Kongt.Entity;
using Kongt.Model;

namespace Kongt.Service
{
    public class ServiceBase
    {
        public ServiceBase()
        {
            context = Entities.getDbContext();
        }

        protected Entities context = null;

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }
    }
}

