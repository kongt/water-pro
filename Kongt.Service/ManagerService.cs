﻿using Kongt.IServices;
using Kongt.Service;
using System;
using Kongt.Model;
using System.Collections.Generic;
using System.Linq;

namespace Kongt.Service
{
    /// <summary>
    /// 区域
    /// </summary>
    public class ManagerService : ServiceBase, IService, IDisposable, IManagerService
    {
        public void Create(ManagerInfo manager)
        {
            context.ManagerInfoes.Add(manager);
            context.SaveChanges();
        }

        public ManagerInfo GetManagerByUserName(string userName)
        {
            return context.ManagerInfoes.FirstOrDefault(item => item.UserName == userName);
        }

        public PageModel<ManagerInfo> Query(QueryBase q)
        {
            var query = context.ManagerInfoes.OrderByDescending(item => item.Id);
            return new PageModel<ManagerInfo>()
            {
                Count = query.Count(),
                Rows = query.Skip(q.Skip).Take(q.PageSize).ToList()
            };
        }
    }
}

