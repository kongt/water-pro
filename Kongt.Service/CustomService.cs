﻿using Kongt.IServices;
using Kongt.Service;
using System;
using Kongt.Model;
using System.Collections.Generic;
using System.Linq;
using Kongt.IServices.QueryModel;
using System.Data.SqlClient;

namespace Kongt.Service
{
    /// <summary>
    /// 区域
    /// </summary>
    public class CustomService : ServiceBase, IService, IDisposable, ICustomService
    {
        public void Create(Customer custom)
        {
            custom.CreateDate = DateTime.Now;
            context.Customers.Add(custom);
            context.SaveChanges();
        }

        public void CreateAccount(OpenAccountInfo account)
        {
            context.OpenAccountInfoes.Add(account);
            context.SaveChanges();
        }

        public void Delete(long id)
        {
            context.Customers.Remove(this.Find(id));
            context.SaveChanges();
        }

        public void DeleteAccount(long id)
        {
            context.OpenAccountInfoes.Remove(this.FindAccount(id));
            context.SaveChanges();
        }

        public Customer Find(long id)
        {
            return context.Customers.Find(id);
        }

        public OpenAccountInfo FindAccount(long id)
        {
            return context.OpenAccountInfoes.Find(id);
        }

        public PageModel<OpenAccountInfo> QueryAccounts(OpenAccountQuery Query)
        {
            var customerIds = new List<long>();
            if (!string.IsNullOrEmpty(Query.customerId))
            {
                customerIds = context.Customers.Where(t => t.CustomerId == Query.customerId).Select(c => c.Id).ToList();
            }
            var query = context.OpenAccountInfoes.Include("OpenAccountDocuments").Where(item =>
            (string.IsNullOrEmpty(Query.customerId) || customerIds.Any(j => j == item.CustomerId)) &&
              Query.UserId == null || item.CustomerId == Query.UserId
            ).OrderByDescending(item => item.Id);
            return new PageModel<OpenAccountInfo>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }

        public PageModel<Customer> QueryCustomers(CustomQuery Query)
        {
            var query = context.Customers.Where(item =>
                 (string.IsNullOrEmpty(Query.CustomerId) || item.CustomerId == Query.CustomerId)
              && (string.IsNullOrEmpty(Query.CustomerName) || item.CustomerName.Contains(Query.CustomerName))
              && (string.IsNullOrEmpty(Query.PersonId) || item.PersonId == Query.PersonId)
              && (string.IsNullOrEmpty(Query.FirstArea) || item.FirstArea == Query.FirstArea)
              && (string.IsNullOrEmpty(Query.SecondArea) || item.SecondArea == Query.SecondArea)
              && (string.IsNullOrEmpty(Query.LinkPhone) || item.LinkPhone == Query.LinkPhone)
              && (string.IsNullOrEmpty(Query.MeterNo) || item.MeterNo == Query.MeterNo)
              && (Query.CustomStat == null || Query.CustomStat == (Customer.CustomStatEnum)item.CustomerStat)
              && (Query.HouseType == null || Query.HouseType == (Customer.HouseTypeEnum)item.HouseType)
              && (Query.MeterNature == null || Query.MeterNature == (Customer.MeterNatureEnum)item.MeterNature)
              && (Query.EmployeeId == null || Query.EmployeeId == item.ReadPersonId)
              && (Query.OpenAccountDate == null || Query.OpenAccountDate == item.OpenAccountDate)
              && (Query.WaterTypeId == null || Query.WaterTypeId == item.WaterTypeId)
              && (Query.MeterStart == null || Query.MeterStart == item.MeterStart)
               ).OrderByDescending(item => item.Id);
            return new PageModel<Customer>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }

        public void Update(Customer custom)
        {
            var entity = this.Find(custom.Id);
            entity.CustomerId = custom.CustomerId;
            entity.CustomerName = custom.CustomerName;
            entity.PersonId = custom.PersonId;
            entity.LinkPhone = custom.LinkPhone;
            entity.FirstArea = custom.FirstArea;
            entity.SecondArea = custom.SecondArea;
            entity.DetailAddress = custom.DetailAddress;
            entity.MeterNo = custom.MeterNo;
            entity.MeterStart = custom.MeterStart;
            entity.CustomerStat = custom.CustomerStat;
            entity.HouseType = custom.HouseType;
            entity.WaterTypeId = custom.WaterTypeId;
            entity.MeterNature = custom.MeterNature;
            entity.ReadPersonId = custom.ReadPersonId;
            entity.OpenAccountDate = custom.OpenAccountDate;
            entity.DESCRIPTION = custom.DESCRIPTION;
            entity.ParentId = custom.ParentId;
            //创建日期不更新
            context.SaveChanges();
        }

        public void UpdateAccount(OpenAccountInfo account)
        {
            context.Database.ExecuteSqlCommand("delete from OpenAccountDocument where openaccountInfoId=@id", new SqlParameter("@id", account.Id));
            if (account.OpenAccountDocuments.Count > 0)
            {
                foreach (var item in account.OpenAccountDocuments)
                {
                    context.Database.ExecuteSqlCommand("INSERT INTO dbo.OpenAccountDocument( OpenAccountInfoId,DocumentName ,DocumentUnit ,DocumentPrice ,DocumentNum ,DocumentDesc)VALUES  ( @OpenAccountInfoId ,@DocumentName,@DocumentUnit,@DocumentPrice,@DocumentNum,@DocumentDesc)"
                                  , new SqlParameter("@OpenAccountInfoId", account.Id)
                                  , new SqlParameter("@DocumentName", item.DocumentName)
                                    , new SqlParameter("@DocumentUnit", item.DocumentUnit)
                                        , new SqlParameter("@DocumentPrice", item.DocumentPrice)
                                             , new SqlParameter("@DocumentNum", item.DocumentNum)
                                              , new SqlParameter("@DocumentDesc", item.DocumentDesc)
                                   );
                }
            }
            var entity = this.FindAccount(account.Id);
            entity.CustomerId = account.CustomerId;
            entity.OpenAccountCharge = account.OpenAccountCharge;
            entity.DocumentCharge = account.DocumentCharge;
            entity.WorkTimeCharge = account.WorkTimeCharge;
            entity.OtherCharge = account.OtherCharge;
            entity.ShouldGetCharge = account.ShouldGetCharge;
            entity.RealyGetCharge = account.RealyGetCharge;
            entity.RestoreCharge = account.RestoreCharge;
            //entity.OperatorId = account.OperatorId;
            //entity.CreateDate = account.CreateDate;
            context.SaveChanges();
            //强制拉取数据库 数据 不然可能存在数据库数据与ef容器数据不一致的情况

        }
    }
}

