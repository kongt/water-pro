﻿using Kongt.IServices;
using Kongt.Service;
using System;
using Kongt.Model;
using System.Collections.Generic;
using System.Linq;
using Kongt.IServices.QueryModel;
using System.Data.SqlClient;

namespace Kongt.Service
{
    /// <summary>
    /// 区域
    /// </summary>
    public class MeterService : ServiceBase, IService, IDisposable, IMeterService
    {
        public void Create(MeterInfo meter)
        {
            context.MeterInfoes.Add(meter);
            context.SaveChanges();
        }



        public void Delete(long id)
        {
            context.MeterInfoes.Remove(this.Find(id));
            context.SaveChanges();
        }

        public void DeleteChangeMeter(long id)
        {
            context.ChangeMeterInfoes.Remove(this.FindChangeMeter(id));
            context.SaveChanges();
        }

        public MeterInfo Find(long id)
        {
            return context.MeterInfoes.Find(id);
        }

        public ChangeMeterInfo FindChangeMeter(long id)
        {
            return context.ChangeMeterInfoes.Find(id);
        }

        public PageModel<ChangeMeterInfo> QueryChangeMeters(ChangeMeterQuery Query)
        {
            var list = new List<long>();
            if (!string.IsNullOrEmpty(Query.CustomerId))
            {
                list = context.Customers.Where(item => item.CustomerId == Query.CustomerId).Select(c => c.Id).ToList();
            }
            var query = context.ChangeMeterInfoes.Where(item => string.IsNullOrEmpty(Query.CustomerId) || list.Contains(item.CustomerId)).OrderByDescending(item => item.Id);
            return new PageModel<ChangeMeterInfo>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }



        public PageModel<MeterInfo> QueryMeters(MeterInfoQuery Query)
        {
            var list = new List<long>();
            var ifstr = (string.IsNullOrEmpty(Query.CustomerId)) && (string.IsNullOrEmpty(Query.CustomerName)) && (string.IsNullOrEmpty(Query.MeterNo))
                      && (string.IsNullOrEmpty(Query.FirstArea)) && (string.IsNullOrEmpty(Query.SecondArea)) && (Query.WaterTypeId == null) && (Query.ReadPersonId == null);
            if (!ifstr)
            {
                list = context.Customers.Where(item =>
                                                     (string.IsNullOrEmpty(Query.CustomerId) || item.CustomerId == Query.CustomerId)
                                                     && (string.IsNullOrEmpty(Query.CustomerName) || item.CustomerName == Query.CustomerName)
                                                     && (string.IsNullOrEmpty(Query.MeterNo) || item.MeterNo == Query.MeterNo)
                                                     && (string.IsNullOrEmpty(Query.FirstArea) || item.FirstArea == Query.FirstArea)
                                                     && (string.IsNullOrEmpty(Query.SecondArea) || item.SecondArea == Query.SecondArea)
                                                     && (Query.WaterTypeId == null || item.WaterTypeId == Query.WaterTypeId)
                                                ).Select(item => item.Id).ToList();
            }
            var query = context.MeterInfoes.Where(item => ((!ifstr) ? list.Contains(item.CustomerId) : true)
                                                         && (Query.ReadMatchDateStart == null || item.ReadMeterDate >= Query.ReadMatchDateStart)
                                                         && (Query.ReadMatchDateEnd == null || item.ReadMeterDate <= Query.ReadMatchDateEnd)
                                                  ).OrderByDescending(item => item.ReadMeterDate);
            return new PageModel<MeterInfo>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }


        public void UpdateChangeMeter(ChangeMeterInfo cm)
        {
            var entity = this.FindChangeMeter(cm.Id);
            entity.CustomerId = cm.CustomerId;
            entity.NewMeterNo = cm.NewMeterNo;
            entity.Operator = cm.Operator;
            entity.ChangeMeterDate = cm.ChangeMeterDate;
            entity.MeterStart = cm.MeterStart;
            entity.ProcesDate = cm.ProcesDate;
            context.SaveChanges();
        }

        public void CreateChangeMeter(ChangeMeterInfo cm)
        {
            context.ChangeMeterInfoes.Add(cm);
            context.SaveChanges();
        }

        public void Update(MeterInfo meter)
        {
            var entity = this.Find(meter.Id);
            entity.CustomerId = meter.CustomerId;
            entity.LastShow = meter.LastShow;
            entity.ThisShow = meter.ThisShow;
            entity.ThisLoseWater = meter.ThisLoseWater;
            entity.ReadMeterDate = meter.ReadMeterDate;
            entity.MeterStat = meter.MeterStat;
            context.SaveChanges();
        }

        public PageModel<Customer> QueryNoReadMeterCustomers(NoReadMeterQuery Query)
        {

            //获取已经抄表的用户ID
            var list = context.MeterInfoes.Where(item =>
                                                      (Query.ReadMatchDateStart == null || item.ReadMeterDate >= Query.ReadMatchDateStart)
                                                     && (Query.ReadMatchDateEnd == null || item.ReadMeterDate <= Query.ReadMatchDateEnd)
                        ).Select(item => item.CustomerId);
            var query = context.Customers.Where(item =>
                                                    !list.Contains(item.Id)
                                                    && (string.IsNullOrEmpty(Query.CustomerId) || item.CustomerId == Query.CustomerId)
                                                    && (string.IsNullOrEmpty(Query.CustomerName) || item.CustomerName == Query.CustomerName)
                                                    && (string.IsNullOrEmpty(Query.MeterNo) || item.MeterNo == Query.MeterNo)
                                                    && (string.IsNullOrEmpty(Query.FirstArea) || item.FirstArea == Query.FirstArea)
                                                    && (string.IsNullOrEmpty(Query.SecondArea) || item.SecondArea == Query.SecondArea)
                                                    && (Query.WaterTypeId == null || item.WaterTypeId == Query.WaterTypeId)
                                                ).OrderByDescending(item => item.Id);
            return new PageModel<Customer>
            {
                Rows = query.Skip(Query.Skip).Take(Query.PageSize).ToList(),
                Count = query.Count()
            };
        }
    }
}

